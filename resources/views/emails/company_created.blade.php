<h2>Company created</h2>

<table>
    <tr>
        <td>Name</td>
        <td>{{$company->name}}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{$company->email}}</td>
    </tr>
    <tr>
        <td>Website</td>
        <td>{{$company->website}}</td>
    </tr>
</table>
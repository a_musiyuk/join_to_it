@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form  action="{{route('company.update',$company->id)}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input class="form-control" type="text" name="name" required value="{{$company->name}}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" type="email" name="email" value="{{$company->email}}">
        </div>
        <div class="form-group">
            <label for="website">Web</label>
            <input class="form-control" type="text" name="website" value="{{$company->website}}">
        </div>
        <div class="form-group">
            <label for="logo">Logo</label>
            <input class="form-control" type="file" name="logo">
        </div>
        <button class="button-primary" type="submit">Save</button>
    </form>
@endsection
@extends('layouts.app')

@section('page_title')
    Companies
@endsection


@push('scripts-header')

@endpush

@push('after_styles')
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
@endpush

@section('content')
    <div class="row">
        <div class="col">
            <a class="button-primary" href="{{route('company.create')}}">Add</a>
        </div>

    </div>

    <div class="row">
        <div class="col">
            <table class="table" id="table-company">
                <thead>
                <td>Name</td>
                <td>E-mail</td>
                <td>Web</td>
                <td>Logo</td>
                <td>Action</td>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <td>{{$company->name}}</td>
                        <td>{{$company->email}}</td>
                        <td>{{$company->website}}</td>
                        <td>@if($company->logo) <img src="/storage/{{$company->logo}}" height="200" width="200">@endif</td>
                        <td>{!! $company->actionButton !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$companies->links()}}
        </div>

    </div>

    <script>
        function deleteRow(id){
            if (confirm("Delete company?")) {
                $.ajaxSetup({
                    headers:{'X-CSRF-Token':$('input[name=_token]').val()}
                });

                $.ajax({
                    url : '/company/'+id,
                    type : 'DELETE',
                    success : function(data) {
                        location.reload();
                    },
                    error: function(e) {
                        console.log(e);
                    }

                });
            }
        }
    </script>
@endsection

@push('scripts-footer')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    var table = $('#table-company').DataTable({
        processing: true,
        serverSide: false,
        paging:false,
    });
</script>
@endpush
@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('company.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input class="form-control" type="text" name="name" required value="{{old('name')}}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control"type="email" name="email" value="{{old("email")}}">
        </div>
        <div class="form-group">
            <label for="website">Web</label>
            <input class="form-control"type="text" name="website" value="{{old("website")}}">
        </div>
        <div class="form-group">
            <label for="logo">Logo</label>
            <input type="file" name="logo">
        </div>

        <button  class="button-primary" type="submit">Save</button>
    </form>
@endsection
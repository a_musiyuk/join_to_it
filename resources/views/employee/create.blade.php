@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('employees.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">First name</label>
            <input class="form-control" type="text" name="first_name" required value="{{old('first_name')}}">
        </div>
        <div class="form-group">
            <label for="name">Last name</label>
            <input class="form-control" type="text" name="last_name" required value="{{old('last_name')}}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" type="email" name="email" value="{{old("email")}}">
        </div>
        <div class="form-group">
            <label for="phone">Phone</label>
            <input class="form-control" type="text" name="phone" value="{{old("phone")}}">
        </div>
        <div class="form-group">
            <label for="company_id">Company</label>
            <select class="form-control" name="company_id">
                @foreach(\App\Company::all() as $company)
                    <option value="{{$company->id}}">{{$company->name}}</option>
                @endforeach
            </select>
        </div>

        <button class="button-primary" type="submit">Save</button>
    </form>
@endsection
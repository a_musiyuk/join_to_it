@extends('layouts.app')

@section('page_title')
    Employees
@endsection

@push('after_styles')
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
@endpush

@push('scripts-header')

@endpush



@section('content')
    <div class="row">
        <div class="col-1 col-offset-11">
            <a href="{{route('employees.create')}}">Add</a>
        </div>

    </div>

    <div class="row">
        <div class="col">
            <table class="table" id="table-employees">
                <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>E-mail</th>
                    <th>Phone</th>
                    <th>Company</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <td>{{$employee->first_name}}</td>
                        <td>{{$employee->last_name}}</td>
                        <td>{{$employee->email}}</td>
                        <td>{{$employee->phone}}</td>
                        <td>{{$employee->company->name}}</td>
                        <td>{!! $employee->actionButton !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$employees->links()}}
        </div>

    </div>

    <script>
        function deleteRow(id){
            if (confirm("Delete employee?")) {
                $.ajaxSetup({
                    headers:{'X-CSRF-Token':$('input[name=_token]').val()}
                });

                $.ajax({
                    url : '/employees/'+id,
                    type : 'DELETE',
                    success : function(data) {
                        location.reload();
                    },
                    error: function(e) {
                        console.log(e);
                    }

                });
            }
        }
    </script>

@endsection

@push('scripts-footer')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
    var table = $('#table-employees').DataTable({
        processing: true,
        serverSide: false,
        paging: false,
    });
</script>
@endpush
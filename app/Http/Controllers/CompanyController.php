<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\CompanyRequest;
use App\Mail\CompanyCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::latest()->paginate(10);
        return view('company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        if ($request->validated()){
            $data = $request->only(['name','email','website']);
            $newCompany = new Company();
            $newCompany->fill($data);
            if ($request->hasFile('logo')) {
                info('have logo');
                $newCompany->uploadLogo($request->file('logo'));
            }

            if ($newCompany->save()) {
                Mail::to($request->user())->send(new CompanyCreated($newCompany));
                return redirect()->route('company.index');
            }else{
                return redirect()->withInput($data)->route('company.create');
            }
        }else{
            return redirect()->withInput($request->all())->route('company.edit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        if ($request->validated()){
            $data = $request->only(['name','email','website']);
            $company->fill($data);
            if ($request->hasFile('logo')) {
                $company->uploadLogo($request->file('logo'));
            }
            if ($company->save()) {
                return redirect()->route('company.index');
            }else{
                return redirect()->withInput($data)->route('company.edit');
            }
        }else{
            return redirect()->withInput($request->all())->route('company.edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $guarded=['id'];



    public function getActionButtonAttribute(){
        return "<a href='".route('company.edit',$this->id)."'>Edit</a><br>
        <a href='#' onclick='deleteRow(".$this->id.")'>Delete</a>";
    }


    public function uploadLogo($file)
    {

        $tmpFileName = time() . '-' . $file->getClientOriginalName();
        info(storage_path('/').$tmpFileName);
        $file->move(storage_path('/app/public/'), $tmpFileName);
        $this->logo = $tmpFileName;
    }
}

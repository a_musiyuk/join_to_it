<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded=['id'];



    public function getActionButtonAttribute(){
        return "<a href='".route('employees.edit',$this->id)."'>Edit</a><br>
        <a href='#'onclick='deleteRow(".$this->id.")'' >Delete</a>";
    }

    public  function company(){
     return $this->belongsTo(Company::class);
    }

}
